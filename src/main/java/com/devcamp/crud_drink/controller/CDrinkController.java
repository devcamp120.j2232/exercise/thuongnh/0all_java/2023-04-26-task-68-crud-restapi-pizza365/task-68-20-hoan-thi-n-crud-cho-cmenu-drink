package com.devcamp.crud_drink.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.crud_drink.model.CDrink;
import com.devcamp.crud_drink.reposioty.IDrinkRepository;

@CrossOrigin
@RestController
public class CDrinkController {

    @Autowired
    IDrinkRepository pDrinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks() {

        try {
            List<CDrink> pdrinks = new ArrayList<CDrink>();
            pDrinkRepository.findAll().forEach(pdrinks::add);
            return new ResponseEntity<>(pdrinks, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Viết method get list drink: getDrinkById(), và chạy test trên postman

    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getDrinkById(@PathVariable long id) {

        try {
            CDrink pdrinks = pDrinkRepository.findById(id);

            return new ResponseEntity<>(pdrinks, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // 7 Viết method create drink: createDrink(), và chạy test trên postman
    @PostMapping("/drinks")
    public ResponseEntity<Object> createDrink(@Valid @RequestBody CDrink pCDrink) {
        try {
            pCDrink.setNgayTao(new Date());
            pCDrink.setNgayCapNhat(null);
            CDrink _voucher = pDrinkRepository.save(pCDrink); // lưu vào sql
            return new ResponseEntity<>(pCDrink, HttpStatus.CREATED); // trả về restquet drink đã thêm

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Error: " + e.getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to create specified drink" + e.getCause().getCause().getMessage());
        }
    }

    // Viết method update drink: updateDrink(), và chạy test trên postman
    @PutMapping("/drinks/{id}")
    public ResponseEntity<Object> updateDrink(@PathVariable long id, @RequestBody CDrink pDrink) {

        try {
            CDrink drinkData = pDrinkRepository.findById(id); // tìm loại nuowcss uống cần sửa
            CDrink drink = drinkData;
            drink.setMaNuocUong(pDrink.getMaNuocUong());
            drink.setTenNuocUong(pDrink.getTenNuocUong());
            drink.setDonGia(pDrink.getDonGia());
            drink.setGhiChu(pDrink.getGhiChu());
            drink.setNgayCapNhat(new Date());
            try {
                return new ResponseEntity<>(pDrinkRepository.save(pDrink), HttpStatus.OK);
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to create specified drink" + e.getCause().getCause().getMessage());
            }

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    // Viết method delete drink: deleteDrink(), xóa theo id
    @DeleteMapping("/drinks/{id}")
    public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") long id) {
        try {
            pDrinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // lỗi máy chủ
        }
    }
    // xóa all 
    @DeleteMapping("/drinks/all")
    public ResponseEntity<CDrink> deleteAllDrink() {
        try {
            pDrinkRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // lỗi máy chủ
        }
    }


}
