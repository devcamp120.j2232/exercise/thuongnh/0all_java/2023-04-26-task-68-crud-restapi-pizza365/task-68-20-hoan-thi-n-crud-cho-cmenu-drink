package com.devcamp.crud_drink.reposioty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.crud_drink.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long>{
    CDrink findById(long id);
}
