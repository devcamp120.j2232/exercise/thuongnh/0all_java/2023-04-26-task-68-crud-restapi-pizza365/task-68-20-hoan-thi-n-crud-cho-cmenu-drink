package com.devcamp.crud_drink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudDrinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudDrinkApplication.class, args);
	}

}
